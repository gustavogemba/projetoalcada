﻿
using Mega.Erp.Empreiteiros.Contratos.Models;
using Mega.Portal.Aprovacoes.Alcadas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace Mega.Portal.Aprovacoes.Alcadas.Core
{

    public class Calculos
    {
        private static Calculos _instance = new Calculos();

        public static Calculos Instance
        {
            get { return _instance; }
        }
        public decimal TotalContrato(IEnumerable<ContratoItem> lItens)
        {
            return lItens.Where(p => !p.Liberado.Equals("S") || p.AprovaAlcada == "AA").Sum(p => p.Valor);
        }

        public IEnumerable<PesoGrupo> PesoTotalGrupos(IEnumerable<ContratoItem> lItens)
        {
            var lPesoGrupo = lItens.GroupBy(p => p.Produto.GrupoEstoque.GrupoCodigo)
                                   .Select(g => new PesoGrupo()
                                   {
                                       GrupoTabela = g.First().Produto.GrupoEstoque.GrupoTabela,
                                       GrupoPadrao = g.First().Produto.GrupoEstoque.GrupoPadrao,
                                       GrupoIdentificador = g.First().Produto.GrupoEstoque.GrupoIdentificador,
                                       GrupoCodigo = g.First().Produto.GrupoEstoque.GrupoCodigo,
                                       Valor = g.Sum(s => s.Valor)
                                   }).OrderByDescending(o => o.Valor);
            return lPesoGrupo;
        }
    }
}
