﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Portal.Aprovacoes.Alcadas.Models;
using Mega.Erp.Empreiteiros.Contratos.Models;

namespace Mega.Portal.Aprovacoes.Alcadas.Core
{
    public class ValidacoesAlcada
    {
        private static ValidacoesAlcada _instance = new ValidacoesAlcada();

        public static ValidacoesAlcada Instance
        {
            get { return _instance; }
        }

        public bool ValidaExcecaoClasseItem(IEnumerable<ContratoItem> listaItem, IEnumerable<ClasseExcecao> listaExcecao)
        {
            foreach (ContratoItem item in listaItem)
            {
                if (listaExcecao.Count(p => p.ClasseTabela == item.Classe.ClasseTabela &&
                                            p.ClasseReduzido == item.Classe.ClasseReduzido &&
                                            p.ClassePadrao == item.Classe.ClassePadrao &&
                                            p.ClasseIdentificador == item.Classe.ClasseIdentificador) > 0)
                {
                    return true;
                }
            }
            return false;
        }

        public decimal? RecuperaExcecaoValida(CentroCusto centroCusto, Projeto projeto, IEnumerable<PerfilAlcadaExcecao> lExcecao)
        {
            for (int i = 0; i < lExcecao.Count(); i++)
            {
                if ((lExcecao.ElementAt(i).CentroCusto.CCustoReduzido.Equals(centroCusto.CCustoReduzido) || !lExcecao.ElementAt(i).CentroCusto.CCustoReduzido.HasValue) &&
                        (lExcecao.ElementAt(i).Projeto.ProjetoReduzido.Equals(projeto.ProjetoReduzido) || !lExcecao.ElementAt(i).Projeto.ProjetoReduzido.HasValue))
                    return lExcecao.ElementAt(i).CodigoExcecao;
            }
            return (decimal?) null;
        }

        public decimal RecuperaFaixaValida(IEnumerable<PerfilAlcadaFaixa> faixas, decimal totalContrato)
        {
            for (int i = 0; i < faixas.Count(); i++)
            {
                if (totalContrato >= faixas.ElementAt(i).ValorInicial && totalContrato <= faixas.ElementAt(i).ValorFinal)
                {
                    return faixas.ElementAt(i).CodigoFaixa;
                }
            }
            return 0;
        }

        public int RecuperarVigenciaValida(IEnumerable<PerfilAlcadaVigencia> alcadaVigencia)
        {
            int vigencia = 0;
            DateTime dataVigencia = DateTime.MinValue;
            for (int i = 0; i < alcadaVigencia.Count(); i++)
            {
                if ((alcadaVigencia.ElementAt(i).DataVigencia.Date <= DateTime.Now.Date) &&
                    (dataVigencia < alcadaVigencia.ElementAt(i).DataVigencia.Date))
                {
                    vigencia = (int)alcadaVigencia.ElementAt(i).CodigoVigencia;
                    dataVigencia = alcadaVigencia.ElementAt(i).DataVigencia;
                }
            }
            return vigencia;
        }

        internal IEnumerable<PerfilAlcadaControle> PopulaListaPerfilControle(Contrato cto, decimal faixa, decimal alcada, IEnumerable<PerfilAlcadaExcecao> lExcecaoCustoProjeto, IEnumerable<RateioBloco> lRateiBloco, bool temExcecaoClasse)
        {
            List<PerfilAlcadaControle> lPopulaListaPerfilControle = new List<PerfilAlcadaControle>();

            if (lRateiBloco.Count() > 1 && !temExcecaoClasse)
            {
                foreach (var rateio in lRateiBloco)
                {
                    decimal? _excecao = lExcecaoCustoProjeto.Count() == 0 ? null : ValidacoesAlcada.Instance.RecuperaExcecaoValida(rateio.CentroCusto, rateio.Projeto, lExcecaoCustoProjeto);
                    lPopulaListaPerfilControle.Add(PopulaObjetoControle(cto, faixa, _excecao, alcada));
                }
            }
            else
            {
                decimal? _excecao = lExcecaoCustoProjeto.Count() == 0 ? null : ValidacoesAlcada.Instance.RecuperaExcecaoValida(cto.CentroCusto, cto.Projeto, lExcecaoCustoProjeto);
                lPopulaListaPerfilControle.Add(PopulaObjetoControle(cto, faixa, _excecao, alcada));
            }

            return lPopulaListaPerfilControle;
        }

        public PerfilAlcada PerfisDeAlcadaAptos(IEnumerable<PerfilAlcada> lPerfilAlcada, IEnumerable<PerfilAlcadaVinculoAplicacao> lVinculoAplicacao, IEnumerable<PerfilAlcadaFilial> lFilial, IEnumerable<PerfilAlcadaGrupoEstoque> lGrupoEstoque)
        {
            var resultado = from listaPerfil in lPerfilAlcada
                            join vinculoAplicacao in lVinculoAplicacao on listaPerfil.CodigoPerfil equals vinculoAplicacao.CodigoAlcada //into _a
                            //from _A in _a.DefaultIfEmpty()
                            join filial in lFilial on listaPerfil.CodigoPerfil equals filial.CodigoPerfil //into _t
                            //from _T in _t.DefaultIfEmpty()             
                            select new PerfilAlcada
                            {
                                CodigoPerfil = listaPerfil.CodigoPerfil,
                                Descricao = listaPerfil.Descricao,
                                CancelarAprovacoes = listaPerfil.CancelarAprovacoes,
                                TipoAprovacao = listaPerfil.TipoAprovacao
                            };
            return resultado.FirstOrDefault();
        }

        public IEnumerable<PerfilAlcada> RecuperaPerfisValidos(IEnumerable<PerfilAlcada> perfis)
        {
            return perfis.Where(p => p.PerfilExcluido.Equals("N") && p.TipoAprovacao.Equals("U"));
        }

        public IEnumerable<PerfilAlcadaVinculoAplicacao> RecuperaVinculoAplicacoesValida(IEnumerable<PerfilAlcadaVinculoAplicacao> aplicacoes, int aplicacaoMovimento)
        {
            return aplicacoes.Where(p => p.AplicacaoAlcada.Equals(aplicacaoMovimento));
        }

        private PerfilAlcadaControle PopulaObjetoControle(Contrato cto, decimal faixa, decimal? excecao, decimal alcada)
        {
            return new PerfilAlcadaControle
            {
                //CodigoControle = GetSequence("mgglo.seq_wglo_perfil_alc_controle"),
                CodigoAplicacao = cto.Origem.Equals("E") ? TipoAplicacao.contratoEmpreiteiros.GetHashCode() : TipoAplicacao.contratoCotacao.GetHashCode(),
                CodigoFaixa = faixa,
                CodigoFaixaGrupo = null,
                CodigoPaiMovimento = null,
                CodigoExcecao = excecao,
                CodigoMovimento = cto.Codigo,
                NivelAnterior = -1,
                NivelAtual = -1,
                DataRegistro = DateTime.Now,
                CodigoPerfil = alcada,
                Parecer = "Armação de alçada teste serviço",
                AprovacaoMultipla = "N",
                Status = "P",
                CodigoUsuarioLiberacao = cto.Usuario
            };
        }
    }
}
