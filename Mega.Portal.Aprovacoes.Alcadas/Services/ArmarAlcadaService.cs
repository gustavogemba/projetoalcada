﻿using Mega.Portal.Aprovacoes.Alcadas.Models;
using Mega.Portal.Aprovacoes.Alcadas.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using Mega.Portal.Aprovacoes.Alcadas.Core;
using Mega.Erp.Empreiteiros.Contratos.Models;
using Mega.Erp.Empreiteiros.Contratos.Service;

namespace Mega.Portal.Aprovacoes.Alcadas.Services
{
    public class ArmarAlcadaService
    {
        private IArmarAlcadaRepository _repository;
        private ContratoService _contrato;

        public ArmarAlcadaService(IArmarAlcadaRepository repository, ContratoService contrato) 
        {
            _repository = repository;
            _contrato = contrato;
        }
        
        public decimal ArmarAlcadaContrato(Contrato contrato)
        {
            if (contrato.Codigo == 0)
                return 0;

            Contrato cto = _contrato.GetContrato(contrato);
            
            return cto.Codigo == 0 ? 0 : ArmaObjetoControle(cto);
        }

        private decimal ArmaObjetoControle(Contrato cto)
        {
            decimal _faixa = 0, _alcada = 0;
            decimal objControle = 0;
            IEnumerable<PesoGrupo> lPesoGrupoEstoque = Calculos.Instance.PesoTotalGrupos(cto.Itens);
            PerfilAlcada perfilApto = VerificaPerfilApto(cto, lPesoGrupoEstoque.ElementAt(0));

            if (perfilApto ==  null || perfilApto.CodigoPerfil == 0) return 0;

            int codigoVigencia = ValidacoesAlcada.Instance.RecuperarVigenciaValida(_repository.GetVigencia(perfilApto.CodigoPerfil));

            if (codigoVigencia == 0) return 0;

            IEnumerable<PerfilAlcadaFaixa> faixas = _repository.GetFaixaValor(codigoVigencia);

            if (faixas.Count() == 0) return 0;

            decimal totalContrato = Calculos.Instance.TotalContrato(cto.Itens);

            _faixa = ValidacoesAlcada.Instance.RecuperaFaixaValida(faixas, totalContrato);

            if (_faixa == 0) return 0;

            _alcada = perfilApto.CodigoPerfil;

            IEnumerable<PerfilAlcadaExcecao> lExcecaoCustoProjeto = _repository.GetExcecao(_faixa);

            IEnumerable<ClasseExcecao> lExcecoesClasse = _repository.GetClasseExcecoes();
            bool temExcecaoClasse = lExcecoesClasse == null ? false : ValidacoesAlcada.Instance.ValidaExcecaoClasseItem(cto.Itens, lExcecoesClasse);

            IEnumerable<RateioBloco> lRateiBloco = _repository.GetRateiosBloco(cto);

            IEnumerable<PerfilAlcadaControle> lperfilControle = ValidacoesAlcada.Instance.PopulaListaPerfilControle(cto, _faixa, _alcada, lExcecaoCustoProjeto, lRateiBloco, temExcecaoClasse);

            foreach (var perfilControle in lperfilControle)
            {
                perfilControle.CodigoControle = GetSequence("mgglo.seq_wglo_perfil_alc_controle");
                objControle = _repository.InsertAlcadaControle(perfilControle);
            }
            return objControle;
        }
        
        private PerfilAlcada VerificaPerfilApto(Contrato cto, PesoGrupo peso)
        {
            //Popula as 4 listas mais relevantes para o processo
            IEnumerable<PerfilAlcada> lPerfilAlcada = ValidacoesAlcada.Instance.RecuperaPerfisValidos(_repository.GetPerfilAlcada());
            IEnumerable<PerfilAlcadaVinculoAplicacao> lVinculoAplicacao = ValidacoesAlcada.Instance.RecuperaVinculoAplicacoesValida(_repository.GetVinculoAplicacao(), cto.Origem.Equals('E') ? TipoAplicacao.contratoEmpreiteiros.GetHashCode() : TipoAplicacao.contratoCotacao.GetHashCode());
            IEnumerable<PerfilAlcadaFilial> lFilial = _repository.GetFilialAlcada(cto);
            IEnumerable <PerfilAlcadaGrupoEstoque> lGrupoEstoque = _repository.GetGrupoEstoqueAlcada(peso);
            
            return ValidacoesAlcada.Instance.PerfisDeAlcadaAptos(lPerfilAlcada, lVinculoAplicacao, lFilial, lGrupoEstoque);
        }

        /// <summary>
        /// Método para recuperar sequence. Obs: Enviar o owner da tabela junto. ex: "mgglo.seq_wglo_perfil_alc_controle".
        /// </summary>
        /// <param name="nomeSequence">Nome da sequence</param>
        /// <returns>Número atual da sequence.</returns>
        private decimal GetSequence(string nomeSequence)
        {
            return _repository.GetSequence(nomeSequence);
        }
    }
}
