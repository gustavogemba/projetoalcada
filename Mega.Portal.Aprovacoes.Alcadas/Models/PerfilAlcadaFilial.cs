﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.Alcadas.Models
{
    public class PerfilAlcadaFilial : ChaveCompleta
    {
        public decimal CodigoPerfil { get; set; }
    }
}
