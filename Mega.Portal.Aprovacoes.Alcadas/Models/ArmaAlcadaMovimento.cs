﻿using Mega.Erp.Empreiteiros.Contratos.Models;
using Mega.Portal.Aprovacoes.Alcadas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.Alcadas.Models
{
    public class ArmaAlcadaMovimento : ChaveCompleta
    {
        public decimal CodigoAplicacao { get; set; }
        public decimal CodigoPerfilAlcada { get; set; }
        public decimal ValorMovimento { get; set; }
        //public decimal pObj_Ser_Tab_In_Codigo { get; set; }
        //public decimal pObj_Ser_In_Sequencia { get; set; }
        public decimal CodigoMovimento { get; set; }
        public decimal CodigoPaiMovimento { get; set; }
        public CentroCusto CentroCusto { get; set; }
        public Projeto Projeto { get; set; }
        public decimal Usuario { get; set; }

    }
}
