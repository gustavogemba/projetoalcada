﻿using Mega.Erp.Empreiteiros.Contratos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.Alcadas.Models
{
    public class RateioBloco : ChaveCompleta
    {
        public decimal CodigoContrato { get; set; }
        public CentroCusto CentroCusto { get; set; }
        public Projeto Projeto { get; set; }
    }
}
