﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.Alcadas.Models
{
    public class PerfilAlcadaFaixa
    {
        public decimal CodigoVigencia { get; set; }
        public decimal CodigoFaixa { get; set; }
        public decimal TotalAssinaturas { get; set; }
        public string ConsideraOrdemAprovacao { get; set; }
        public decimal ValorInicial { get; set; }
        public decimal ValorFinal { get; set; }
        public IEnumerable<PerfilAlcadaExcecao> Excecao { get; set; }
    }
}
