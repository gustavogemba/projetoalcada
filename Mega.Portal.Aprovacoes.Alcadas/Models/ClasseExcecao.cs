﻿using Mega.Erp.Empreiteiros.Contratos.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.Alcadas.Models
{
    public class ClasseExcecao : Classe
    {
        public decimal CodigoExcecao { get; set; }
    }
}
