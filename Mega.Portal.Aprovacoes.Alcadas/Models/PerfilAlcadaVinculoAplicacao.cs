﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.Alcadas.Models
{
    public enum TipoAplicacao {
        contratoEmpreiteiros = 4,
        contratoCotacao = 5
    }

    public class PerfilAlcadaVinculoAplicacao
    {
        public decimal CodigoAlcada { get; set; }
        public decimal AplicacaoAlcada { get; set; }
        public IEnumerable<PerfilAlcadaAplicacao> Aplicacao { get; set; }
    }
}
