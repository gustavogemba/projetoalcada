﻿namespace Mega.Erp.Empreiteiros.Contratos.Models
{
    public class Classe
    {
        public decimal ClasseTabela { get; set; }
        public decimal ClassePadrao { get; set; }
        public string  ClasseIdentificador { get; set; }
        public decimal ClasseReduzido { get; set; }
    }
}