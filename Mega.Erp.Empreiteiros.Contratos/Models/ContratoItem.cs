﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Empreiteiros.Contratos.Models
{
    public class ContratoItem : ChaveCompleta
    {
        public decimal Id { get; set; }
        public decimal Contrato { get; set; }
        public decimal Item { get; set; }
        public decimal Aditivo { get; set; }
        public decimal Valor { get; set; }
        public string Situacao { get; set; }
        public string Liberado { get; set; }
        public string AprovaAlcada { get; set; }
        public Produto Produto { get; set; }
        public Classe Classe { get; set; }
    }
}
