﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Erp.Empreiteiros.Contratos.Models
{
    public class ChaveCompleta
    {
        public decimal CodigoTabela { get; set; }
        public decimal CodigoPadrao { get; set; }
        public decimal CodigoOrganizacao { get; set; }
        public string  CodigoAuxiliar { get; set; }
    }
}
