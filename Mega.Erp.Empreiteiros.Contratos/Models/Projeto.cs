﻿namespace Mega.Erp.Empreiteiros.Contratos.Models
{
    public class Projeto
    {
        public decimal? ProjetoTabela { get; set; }
        public decimal? ProjetoPadrao { get; set; }
        public string  ProjetoIdentificador { get; set; }
        public decimal? ProjetoReduzido { get; set; }
    }
}