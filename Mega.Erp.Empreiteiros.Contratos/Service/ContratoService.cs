﻿using Mega.Erp.Empreiteiros.Contratos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Empreiteiros.Contratos.Models;

namespace Mega.Erp.Empreiteiros.Contratos.Service
{
    public class ContratoService
    {
        private IContratoRepository _repository;

        public ContratoService(IContratoRepository repository)
        {
            _repository = repository;
        }

        public Contrato GetContrato(Contrato contrato)
        {
            Contrato cto = _repository.GetContrato(contrato);
            cto.Itens = _repository.GetItensContrato(contrato);
            GetGrupoEstoque(cto.Itens);
            return cto;
        }

        private void GetGrupoEstoque(IEnumerable<ContratoItem> itens)
        {           
            if (itens.Count() == 0) return;

            foreach (ContratoItem item in itens)
            {
                item.Produto.GrupoEstoque = _repository.GetGrupoEstoque(item);
            }
        }
    }
}
