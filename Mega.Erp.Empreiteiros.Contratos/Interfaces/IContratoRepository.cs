﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Erp.Empreiteiros.Contratos.Models;

namespace Mega.Erp.Empreiteiros.Contratos.Interfaces
{
    public interface IContratoRepository
    {
        Contrato GetContrato(Contrato contrato);
        IEnumerable<ContratoItem> GetItensContrato(Contrato contrato);
        GrupoEstoque GetGrupoEstoque(ContratoItem item);
    }
}
