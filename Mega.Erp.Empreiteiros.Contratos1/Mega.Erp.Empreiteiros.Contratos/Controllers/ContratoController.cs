﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Mega.Portal.Aprovacoes.ArmarAlcadas.Models;
using Mega.Erp.Empreiteiros.Contratos.Service;

namespace Mega.Erp.Empreiteiros.Contratos.Controllers
{
    [Route("api/[controller]")]
    public class ContratoController : Controller
    {
        private ContratoService _service;
        public ContratoController(ContratoService service)
        {
            _service = service;
        }

        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        [HttpPost]
        public Contrato Post([FromBody]Contrato contrato)
        {
            return _service.GetContrato(contrato);
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
