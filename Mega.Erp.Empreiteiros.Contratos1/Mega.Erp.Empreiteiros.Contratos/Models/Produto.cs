﻿namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class Produto
    {
        public decimal ProdutoTabela { get; set; }
        public decimal ProdutoPadrao { get; set; }
        public decimal ProdutoCodigo { get; set; }
        public GrupoEstoque GrupoEstoque { get; set; }
    }
}