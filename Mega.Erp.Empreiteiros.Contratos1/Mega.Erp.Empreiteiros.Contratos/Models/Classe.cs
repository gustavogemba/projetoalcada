﻿namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class Classe
    {
        public decimal ClasseTabela { get; set; }
        public decimal ClassePadrao { get; set; }
        public string  ClasseIdentificador { get; set; }
        public decimal ClasseReduzido { get; set; }
    }
}