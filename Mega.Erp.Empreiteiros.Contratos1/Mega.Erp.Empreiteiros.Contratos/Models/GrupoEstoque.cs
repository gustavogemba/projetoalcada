﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class GrupoEstoque
    {
        public decimal GrupoTabela { get; set; }
        public decimal GrupoPadrao { get; set; }
        public string  GrupoIdentificador { get; set; }
        public decimal GrupoCodigo { get; set; }

    }
}
