﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class CentroCusto
    {
        public decimal? CCustoTabela { get; set; }
        public decimal? CCustoPadrao { get; set; }
        public string CCustoIdentificador { get; set; }
        public decimal? CCustoReduzido { get; set; }
    }
}
