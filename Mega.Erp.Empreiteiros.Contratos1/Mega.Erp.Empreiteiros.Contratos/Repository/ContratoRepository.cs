﻿using Mega.Erp.Empreiteiros.Contratos.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Mega.Portal.Aprovacoes.ArmarAlcadas.Models;
using Dapper;

namespace Mega.Erp.Empreiteiros.Contratos.Repository
{
    public class ContratoRepository : IContratoRepository
    {
        private DbConnection _conn;

        public ContratoRepository(DbConnection conn)
        {
            _conn = conn;
        }

        public Contrato GetContrato(Contrato cto)
        {
            return _conn.Query<Contrato, CentroCusto, Projeto, Contrato>(@"select  c.cto_in_codigo as id
                                                                                  ,c.org_tab_in_codigo as CodigoTabela 
                                                                                  ,c.org_pad_in_codigo as CodigoPadrao 
                                                                                  ,c.org_in_codigo as CodigoOrganizacao
                                                                                  ,c.org_tau_st_codigo as CodigoAuxiliar
                                                                                  ,c.cto_in_codigo as Codigo
                                                                                  ,c.id_usuario_alteracao as Usuario
                                                                                  ,c.fil_in_codigo as Filial
                                                                                  ,c.cto_ch_origem as Origem
                                                                                  ,c.cus_tab_in_codigo as CCustoTabela 
                                                                                  ,c.cus_pad_in_codigo as CCustoPadrao 
                                                                                  ,c.cus_ide_st_codigo as CCustoIdentificador
                                                                                  ,c.cus_in_reduzido as CCustoReduzido
                                                                                  ,c.pro_tab_in_codigo as ProjetoTabela 
                                                                                  ,c.pro_pad_in_codigo as ProjetoPadrao 
                                                                                  ,c.pro_ide_st_codigo as ProjetoIdentificador
                                                                                  ,c.pro_in_reduzido as ProjetoReduzido
                                                                          from mgemp.emp_contrato c
                                                                          where c.org_tab_in_codigo   = :CodigoTabela
                                                                              and c.org_pad_in_codigo = :CodigoPadrao
                                                                              and c.org_in_codigo     = :CodigoOrganizacao
                                                                              and c.org_tau_st_codigo = :CodigoAuxiliar
                                                                              and c.cto_in_codigo     = :Codigo",
                                                                                  (contrato, centrodecusto, projeto) =>
                                                                                  {
                                                                                      contrato.CentroCusto = centrodecusto;
                                                                                      contrato.Projeto = projeto;
                                                                                      return contrato;
                                                                                  }, new
                                                                                  {
                                                                                      CodigoTabela = cto.CodigoTabela
                                                                                         ,
                                                                                      CodigoPadrao = cto.CodigoPadrao
                                                                                         ,
                                                                                      CodigoOrganizacao = cto.CodigoOrganizacao
                                                                                         ,
                                                                                      CodigoAuxiliar = cto.CodigoAuxiliar
                                                                                         ,
                                                                                      Codigo = cto.Codigo
                                                                                  }
                                                                                  , splitOn: "CCustoTabela, ProjetoTabela").SingleOrDefault();
        }

        public IEnumerable<ContratoItem> GetItensContrato(Contrato cto)
        {
            return _conn.Query<ContratoItem, Produto, Classe, ContratoItem>(@"select  c.ctoit_in_codigo as Id
                                                                                     ,c.org_tab_in_codigo as CodigoTabela 
                                                                                     ,c.org_pad_in_codigo as CodigoPadrao 
                                                                                     ,c.org_in_codigo as CodigoOrganizacao
                                                                                     ,c.org_tau_st_codigo as CodigoAuxiliar
                                                                                     ,c.cto_in_codigo as Contrato
                                                                                     ,c.ctoit_in_codigo as Item
                                                                                     ,c.adi_in_codigo as Aditivo
                                                                                     ,c.ctoit_re_valor as Valor
                                                                                     ,c.ctoit_ch_situacao as Situacao
                                                                                     ,c.ctoit_bo_liberado as Liberado
                                                                                     ,c.ctoit_ch_aprovalcada as AprovaAlcada
                                                                                     ,c.pro_tab_in_codigo as ProdutoTabela
                                                                                     ,c.pro_pad_in_codigo as ProdutoPadrao
                                                                                     ,c.pro_in_codigo as ProdutoCodigo
                                                                                     ,c.cla_tab_in_codigo as ClasseTabela
                                                                                     ,c.cla_pad_in_codigo as ClassePadrao
                                                                                     ,c.cla_ide_st_codigo as ClasseIdentificador
                                                                                     ,c.cla_in_reduzido as ClasseReduzido
                                                                                from mgemp.emp_contrato_item c
                                                                              where c.org_tab_in_codigo   = :CodigoTabela
                                                                                  and c.org_pad_in_codigo = :CodigoPadrao
                                                                                  and c.org_in_codigo     = :CodigoOrganizacao
                                                                                  and c.org_tau_st_codigo = :CodigoAuxiliar
                                                                                  and c.cto_in_codigo     = :Codigo
                                                                                  and c.adi_in_codigo is null",
                                                                                      (contratoitem, produto, classe) =>
                                                                                      {
                                                                                          contratoitem.Produto = produto;
                                                                                          contratoitem.Classe = classe;
                                                                                          return contratoitem;
                                                                                      }, new
                                                                                      {
                                                                                          CodigoTabela = cto.CodigoTabela
                                                                                             ,
                                                                                          CodigoPadrao = cto.CodigoPadrao
                                                                                             ,
                                                                                          CodigoOrganizacao = cto.CodigoOrganizacao
                                                                                             ,
                                                                                          CodigoAuxiliar = cto.CodigoAuxiliar
                                                                                             ,
                                                                                          Codigo = cto.Codigo
                                                                                      }
                                                                                      , splitOn: "ProdutoTabela, ClasseTabela");
        }
    }
}
