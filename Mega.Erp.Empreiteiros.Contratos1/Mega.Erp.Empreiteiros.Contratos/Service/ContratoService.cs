﻿using Mega.Erp.Empreiteiros.Contratos.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Portal.Aprovacoes.ArmarAlcadas.Models;

namespace Mega.Erp.Empreiteiros.Contratos.Service
{
    public class ContratoService
    {
        private IContratoRepository _repository;

        public ContratoService(IContratoRepository repository)
        {
            _repository = repository;
        }

        internal Contrato GetContrato(Contrato contrato)
        {
            Contrato oContrato = _repository.GetContrato(contrato);
            oContrato.Itens = _repository.GetItensContrato(contrato);
            return oContrato;
        }
    }
}
