﻿using Mega.Portal.Aprovacoes.ArmarAlcadas.Models;
using Mega.Portal.Aprovacoes.ArmarAlcadas.Interfaces;
using System;
using System.Linq;
using System.Collections.Generic;
using Mega.Portal.Aprovacoes.ArmarAlcadas.Core;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Services
{
    public class ArmarAlcadaService
    {
        private IArmarAlcadaRepository _repository;

        public ArmarAlcadaService(IArmarAlcadaRepository repository) 
        {
            _repository = repository;
        }
        
        public decimal ArmarAlcadaContrato(Contrato contrato)
        {
            if (contrato == null)
                return 0;

            //Contrato oContrato = _repository.GetContrato(contrato);
            //if (oContrato == null)
            //    return 0;
            //IEnumerable<ContratoItem> lItens = CarregarItens(contrato);
            //if (lItens == null)
            //    return 0;

            IEnumerable<ClasseExcecao> lExcecoes = _repository.GetClasseExcecoes();

            //bool temExcecaoClasse = lExcecoes == null ? false : ValidacoesAlcada.Instance.ValidaExcecaoClasseItem(lItens, lExcecoes);
            //decimal totalContrato = Calculos.Instance.TotalContrato(lItens);
            //IEnumerable<PesoGrupo> lPesoGrupoEstoque = Calculos.Instance.PesoTotalGrupos(lItens);
            //PerfilAlcada perfilApto = VerificaPerfilApto(oContrato, lPesoGrupoEstoque.ElementAt(0));

            //return ArmaObjetoControle(oContrato, perfilApto, totalContrato, temExcecaoClasse);
            return 0;
        }

        private decimal ArmaObjetoControle(Contrato oContrato, PerfilAlcada perfilApto, decimal totalContrato, bool temExcecaoClasse)
        {
            decimal _faixa = 0, _alcada = 0;
            decimal? _excecao = null;
            decimal objControle = 0;

            int codigoVigencia = ValidacoesAlcada.Instance.RecuperarVigenciaValida(_repository.GetVigencia(perfilApto.CodigoPerfil));

            if (codigoVigencia == 0) return 0;

            IEnumerable<PerfilAlcadaFaixa> faixas = _repository.GetFaixaValor(codigoVigencia);

            if (faixas.Count() == 0) return 0;

            _faixa = ValidacoesAlcada.Instance.RecuperaFaixaValida(faixas, totalContrato);

            if (_faixa == 0) return 0;

            _alcada = perfilApto.CodigoPerfil;

            IEnumerable<PerfilAlcadaExcecao> lExcecao = _repository.GetExcecao(_faixa);

            if (temExcecaoClasse)
            {
                _excecao = lExcecao.Count() == 0 ? null : ValidacoesAlcada.Instance.RecuperaExcecaoValida(oContrato.CentroCusto, oContrato.Projeto, lExcecao);
                PerfilAlcadaControle perfilControle = PopulaObjetoControle(oContrato, _faixa, _excecao, _alcada);
                objControle = _repository.InsertAlcadaControle(perfilControle);
            }
            else
            {
                IEnumerable<RateioBloco> lRateiBloco = _repository.GetRateiosBloco(oContrato);
                if (lRateiBloco.Count() == 0) return 0;

                for (int i = 0; i < lRateiBloco.Count(); i++)
                {
                    _excecao = lExcecao.Count() == 0 ? null : ValidacoesAlcada.Instance.RecuperaExcecaoValida(lRateiBloco.ElementAt(i).CentroCusto, lRateiBloco.ElementAt(i).Projeto, lExcecao);
                    PerfilAlcadaControle perfilControle = PopulaObjetoControle(oContrato, _faixa, _excecao, _alcada);
                    objControle = _repository.InsertAlcadaControle(perfilControle);
                }
            }
            return objControle;
        }

        //private IEnumerable<ContratoItem> CarregarItens(Contrato contrato)
        //{
        //    IEnumerable<ContratoItem> lItens = _repository.GetContratoItens(contrato);

        //    foreach (ContratoItem item in lItens)
        //    {
        //        item.Produto.GrupoEstoque = _repository.GetGrupoEstoque(item);
        //    }
        //    return lItens;
        //}

        private PerfilAlcada VerificaPerfilApto(Contrato oContrato, PesoGrupo peso)
        {
            //Popula as 4 listas mais relevantes para o processo
            IEnumerable<PerfilAlcada> lPerfilAlcada = ValidacoesAlcada.Instance.RecuperaPerfisValidos(_repository.GetPerfilAlcada());
            IEnumerable<PerfilAlcadaVinculoAplicacao> lVinculoAplicacao = ValidacoesAlcada.Instance.RecuperaVinculoAplicacoesValida(_repository.GetVinculoAplicacao(), oContrato.Origem.Equals('E') ? TipoAplicacao.contratoEmpreiteiros.GetHashCode() : TipoAplicacao.contratoCotacao.GetHashCode());
            IEnumerable<PerfilAlcadaFilial> lFilial = _repository.GetFilialAlcada(oContrato);
            IEnumerable <PerfilAlcadaGrupoEstoque> lGrupoEstoque = _repository.GetGrupoEstoqueAlcada(peso);

            return ValidacoesAlcada.Instance.PerfisDeAlcadaAptos(lPerfilAlcada, lVinculoAplicacao, lFilial, lGrupoEstoque);
        }

        private PerfilAlcadaControle PopulaObjetoControle(Contrato oContrato, decimal faixa, decimal? excecao, decimal alcada)
        {
            return new PerfilAlcadaControle
            {
                CodigoControle = GetSequence("mgglo.seq_wglo_perfil_alc_controle"),
                CodigoAplicacao = oContrato.Origem.Equals("E") ? TipoAplicacao.contratoEmpreiteiros.GetHashCode() : TipoAplicacao.contratoCotacao.GetHashCode(),
                CodigoFaixa = faixa,
                CodigoFaixaGrupo = null,
                CodigoPaiMovimento = null,
                CodigoExcecao = excecao,
                CodigoMovimento = oContrato.Codigo,
                NivelAnterior = -1,
                NivelAtual = -1,
                DataRegistro = DateTime.Now,
                CodigoPerfil = alcada,
                Parecer = "Armação de alçada teste serviço",
                AprovacaoMultipla = "N",
                Status = "P",
                CodigoUsuarioLiberacao = oContrato.Usuario
            };
        }

        /// <summary>
        /// Método para recuperar sequence. Obs: Enviar o owner da tabela junto. ex: "mgglo.seq_wglo_perfil_alc_controle".
        /// </summary>
        /// <param name="nomeSequence">Nome da sequence</param>
        /// <returns>Número atual da sequence.</returns>
        private decimal GetSequence(string nomeSequence)
        {
            return _repository.GetSequence(nomeSequence);
        }
    }
}
