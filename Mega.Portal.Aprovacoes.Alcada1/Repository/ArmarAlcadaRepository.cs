﻿using Dapper;
using Mega.Portal.Aprovacoes.ArmarAlcadas.Interfaces;
using Mega.Portal.Aprovacoes.ArmarAlcadas.Models;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Repository
{
    public class ArmarAlcadaRepository : IArmarAlcadaRepository
    {

        private DbConnection _conn;

        public ArmarAlcadaRepository(DbConnection conn)
        {
            _conn = conn;
        }

        public IEnumerable<ClasseExcecao> GetClasseExcecoes()
        {
            return _conn.Query<ClasseExcecao>(@"select t.ecl_in_codigo as CodigoExcecao 
                                                      ,t.cla_tab_in_codigo as ClasseTabela
                                                      ,t.cla_pad_in_codigo as ClassePadrao
                                                      ,t.cla_ide_st_codigo ClasseIdentificador
                                                      ,t.cla_in_reduzido as ClasseReduzido
                                                 from mgglo.WGLO_EXCECAO_CLASSE t");
        }

        public IEnumerable<RateioBloco> GetRateiosBloco(Contrato cto)
        {
            return _conn.Query<RateioBloco, CentroCusto, Projeto, RateioBloco>(@"SELECT blo.org_tab_in_codigo as CodigoTabela 
                                                                                        ,blo.org_pad_in_codigo as CodigoPadrao 
                                                                                        ,blo.org_in_codigo     as CodigoOrganizacao
                                                                                        ,blo.org_tau_st_codigo as CodigoAuxiliar
                                                                                        ,blo.cto_in_codigo     as CodigoContrato
                                                                                        ,blo.cus_tab_in_codigo as CCustoTabela 
                                                                                        ,blo.cus_pad_in_codigo as CCustoPadrao 
                                                                                        ,blo.cus_ide_st_codigo as CCustoIdentificador
                                                                                        ,blo.cus_in_reduzido   as   CCustoReduzido
                                                                                        ,blo.pro_tab_in_codigo as ProjetoTabela 
                                                                                        ,blo.pro_pad_in_codigo as ProjetoPadrao 
                                                                                        ,blo.pro_ide_st_codigo as ProjetoIdentificador
                                                                                        ,blo.pro_in_reduzido   as   ProjetoReduzido
                                                                                    FROM mgemp.emp_contrato_bloco blo
                                                                                    WHERE blo.org_tab_in_codigo = :CodigoTabela
                                                                                      AND blo.org_pad_in_codigo = :CodigoPadrao
                                                                                      AND blo.org_in_codigo     = :CodigoOrganizacao
                                                                                      AND blo.org_tau_st_codigo = :CodigoAuxiliar
                                                                                      AND blo.cto_in_codigo     = :Codigo
                                                                                    UNION ALL
                                                                                    SELECT tbl.org_tab_in_codigo as CodigoTabela 
                                                                                          ,tbl.org_pad_in_codigo as CodigoPadrao 
                                                                                          ,tbl.org_in_codigo     as CodigoOrganizacao
                                                                                          ,tbl.org_tau_st_codigo as CodigoAuxiliar
                                                                                          ,tbl.cto_in_codigo     as CodigoContrato
                                                                                          ,tbl.cus_tab_in_codigo as CCustoTabela 
                                                                                          ,tbl.cus_pad_in_codigo as CCustoPadrao 
                                                                                          ,tbl.cus_ide_st_codigo as CCustoIdentificador
                                                                                          ,tbl.cus_in_reduzido   as   CCustoReduzido
                                                                                          ,tbl.pro_tab_in_codigo as ProjetoTabela 
                                                                                          ,tbl.pro_pad_in_codigo as ProjetoPadrao 
                                                                                          ,tbl.pro_ide_st_codigo as ProjetoIdentificador
                                                                                          ,tbl.pro_in_reduzido   as   ProjetoReduzido
                                                                                    FROM mgemp.emp_contrato_tercbloco tbl
                                                                                    WHERE tbl.org_tab_in_codigo = :CodigoTabela
                                                                                      AND tbl.org_pad_in_codigo = :CodigoPadrao
                                                                                      AND tbl.org_in_codigo     = :CodigoOrganizacao
                                                                                      AND tbl.org_tau_st_codigo = :CodigoAuxiliar
                                                                                      AND tbl.cto_in_codigo     = :Codigo",
                                                                                   (rateiobloco, centrodecusto, projeto) =>
                                                                                   {
                                                                                       rateiobloco.CentroCusto = centrodecusto;
                                                                                       rateiobloco.Projeto = projeto;
                                                                                       return rateiobloco;
                                                                                   }, new
                                                                                   {
                                                                                       CodigoTabela = cto.CodigoTabela,
                                                                                       CodigoPadrao = cto.CodigoPadrao,
                                                                                       CodigoOrganizacao = cto.CodigoOrganizacao,
                                                                                       CodigoAuxiliar = cto.CodigoAuxiliar,
                                                                                       Codigo = cto.Codigo
                                                                                   }
                                                                                   , splitOn: "CCustoTabela, ProjetoTabela");
        }

        public IEnumerable<PerfilAlcada> GetPerfilAlcada()
        {
            return _conn.Query<PerfilAlcada>(@"select cpa_in_codigo as CodigoPerfil           
                                                     ,cpa_st_descricao as Descricao        
                                                     ,cpa_bo_cancelar_aprovacoes as CancelarAprovacoes 
                                                     ,cpa_ch_tipo_aprovacao as TipoAprovacao
                                                 from mgglo.wglo_perfil_alcada pa
                                                 where pa.cpa_bo_excluido <> 'S'
                                                 and pa.cpa_ch_tipo_aprovacao = 'U'");
        }

        public IEnumerable<PerfilAlcadaVigencia> GetVigencia(decimal codigoPerfil)
        {
            return _conn.Query<PerfilAlcadaVigencia>(@" select cpa_in_codigo   as CodigoAlcada
                                                              ,cpv_in_codigo   as CodigoVigencia      
                                                              ,cpv_dt_vigencia as DataVigencia   
                                                         from mgglo.wglo_perfil_alcada_vigencia pv
                                                         where pv.cpa_in_codigo = :codigoPerfil
                                                           and pv.cpv_bo_ativa = 'S'
                                                           and pv.cpv_bo_excluida ='N'"
                                                        , new { codigoPerfil });
        }

        public IEnumerable<PerfilAlcadaFaixa> GetFaixaValor(decimal codigoVigencia)
        {
            return _conn.Query<PerfilAlcadaFaixa>(@" select cpv_in_codigo   as   CodigoVigencia         
                                                           ,cpf_in_codigo   as  CodigoFaixa        
                                                           ,cpf_in_total_assinaturas as TotalAssinaturas  
                                                           ,cpf_bo_considera_ordem_aprov as ConsideraOrdemAprovacao
                                                           ,cpf_re_valor_inicial     as   ValorInicial
                                                           ,cpf_re_valor_final       as      ValorFinal              
                                                      from  mgglo.wglo_perfil_alcada_faixa pf
                                                      where pf.cpv_in_codigo = :codigoVigencia"
                                                      , new { codigoVigencia });
        }

        public IEnumerable<PerfilAlcadaExcecao> GetExcecao(decimal codigoFaixa)
        {
            return _conn.Query<PerfilAlcadaExcecao, Projeto, CentroCusto, PerfilAlcadaExcecao>(@" select cpe_in_codigo  as CodigoExcecao           
                                                                                                      ,cpf_in_codigo as CodigoFaixa           
                                                                                                      ,cpe_in_total_assinaturas as TotalAssinaturas 
                                                                                                      ,cpe_bo_considera_ordem_aprov as ConsideraOrdemAprovacao
                                                                                                      ,pro_tab_in_codigo as ProjetoTabela         
                                                                                                      ,pro_pad_in_codigo as ProjetoPadrao         
                                                                                                      ,pro_ide_st_codigo as ProjetoIdentificador         
                                                                                                      ,pro_in_reduzido   as ProjetoReduzido         
                                                                                                      ,cus_tab_in_codigo as CCustoTabela         
                                                                                                      ,cus_pad_in_codigo as CCustoPadrao         
                                                                                                      ,cus_ide_st_codigo as CCustoIdentificador         
                                                                                                      ,cus_in_reduzido   as CCustoReduzido         
                                                                                                from  mgglo.wglo_perfil_alcada_excecao pe
                                                                                                where pe.cpf_in_codigo = :codigoFaixa"
                                                                                                , (alcadaExcecao, projeto, centrodecusto) =>
                                                                                                {
                                                                                                    alcadaExcecao.Projeto = projeto;
                                                                                                    alcadaExcecao.CentroCusto = centrodecusto;

                                                                                                    return alcadaExcecao;
                                                                                                }
                                                                                                , new { codigoFaixa }
                                                                                                , splitOn: "ProjetoTabela, CCustoTabela");
        }

        //public GrupoEstoque GetGrupoEstoque(ContratoItem item)
        //{
        //    return _conn.Query<GrupoEstoque>(@"select p.gru_tab_in_codigo as GrupoTabela
        //                                             ,p.gru_pad_in_codigo as GrupoPadrao
        //                                             ,p.gru_ide_st_codigo as GrupoIdentificador
        //                                             ,p.gru_in_codigo     as GrupoCodigo
        //                                       from mgadm.Est_Produtos p
        //                                       where p.pro_tab_in_codigo = :proTab
        //                                         and p.pro_pad_in_codigo = :proPad
        //                                         and p.pro_in_codigo     = :proCod",
        //                                        new
        //                                        {
        //                                            proTab = item.Produto.ProdutoTabela,
        //                                            proPad = item.Produto.ProdutoPadrao,
        //                                            proCod = item.Produto.ProdutoCodigo
        //                                        }).FirstOrDefault();
        //}

        public IEnumerable<PerfilAlcadaVinculoAplicacao> GetVinculoAplicacao()
        {
            return _conn.Query<PerfilAlcadaVinculoAplicacao>(@"select t.cpa_in_codigo as CodigoAlcada
                                                                     ,t.pap_in_codigo as AplicacaoAlcada
                                                               from mgglo.WGLO_PERFIL_ALCADA_VINC_APLIC t
                                                               where t.pap_in_codigo = :aplicacao");
        }

        public IEnumerable<PerfilAlcadaFilial> GetFilialAlcada(Contrato oContrato)
        {
            return _conn.Query<PerfilAlcadaFilial>(@"select t.org_tab_in_codigo CodigoTabela 
                                                           ,t.org_pad_in_codigo as CodigoPadrao 
                                                           ,t.fil_in_codigo as CodigoOrganizacao
                                                           ,t.org_tau_st_codigo as CodigoAuxiliar
                                                           ,t.cpa_in_codigo as CodigoPerfil
                                                       from mgglo.WGLO_PERFIL_ALCADA_FILIAL t
                                                      where t.org_tab_in_codigo = :tab
                                                        and t.org_pad_in_codigo = :pad
                                                        and t.fil_in_codigo     = :fil
                                                        and t.org_tau_st_codigo = :tau"
                                                   , new { tab = oContrato.CodigoTabela,
                                                           pad = oContrato.CodigoPadrao,
                                                           fil = oContrato.Filial,
                                                           tau = oContrato.CodigoAuxiliar });
        }

        public decimal InsertAlcadaControle(PerfilAlcadaControle perfilControle)
        {
            _conn.Execute(@"insert into mgglo.wglo_perfil_alcada_controle(CPC_IN_CODIGO
                                                                         ,PAP_IN_CODIGO
                                                                         ,CPF_IN_CODIGO
                                                                         ,CPE_IN_CODIGO
                                                                         ,CPC_IN_CODIGO_OBJETO
                                                                         ,CPC_IN_NIVEL_ANTERIOR
                                                                         ,CPC_IN_NIVEL_ATUAL
                                                                         ,CPC_DT_REGISTRO
                                                                         ,CPA_IN_CODIGO
                                                                         ,CPC_ST_PARECER
                                                                         ,CPC_CH_APROV_MULTIPLA
                                                                         ,PAI_CPC_IN_CODIGO_OBJETO
                                                                         ,GFA_IN_CODIGO
                                                                         ,CPC_CH_STATUS
                                                                         ,GRU_IN_CODIGO)
                                                                  values( :CodigoControle
                                                                         ,:CodigoAplicacao
                                                                         ,:CodigoFaixa
                                                                         ,:CodigoExcecao
                                                                         ,:CodigoMovimento
                                                                         ,:NivelAnterior
                                                                         ,:NivelAtual
                                                                         ,:DataRegistro
                                                                         ,:CodigoPerfil
                                                                         ,:Parecer
                                                                         ,:AprovacaoMultipla
                                                                         ,:PaiCodigoObjeto
                                                                         ,:CodigoFaixaGrupo
                                                                         ,:Status
                                                                         ,:CodigoUsuarioLiberacao)"
                                                                         , new
                                                                         {
                                                                             CodigoControle = perfilControle.CodigoControle,
                                                                             CodigoAplicacao = perfilControle.CodigoAplicacao,
                                                                             CodigoFaixa = perfilControle.CodigoFaixa,
                                                                             CodigoExcecao = perfilControle.CodigoExcecao,
                                                                             CodigoMovimento = perfilControle.CodigoMovimento,
                                                                             NivelAnterior = perfilControle.NivelAnterior,
                                                                             NivelAtual = perfilControle.NivelAtual,
                                                                             DataRegistro = perfilControle.DataRegistro,
                                                                             CodigoPerfil = perfilControle.CodigoPerfil,
                                                                             Parecer = perfilControle.Parecer,
                                                                             AprovacaoMultipla = perfilControle.AprovacaoMultipla,
                                                                             PaiCodigoObjeto = perfilControle.CodigoPaiMovimento,
                                                                             CodigoFaixaGrupo = perfilControle.CodigoFaixaGrupo,
                                                                             Status = perfilControle.Status,
                                                                             CodigoUsuarioLiberacao = perfilControle.CodigoUsuarioLiberacao
                                                                         });
            return perfilControle.CodigoControle;
        }

        public decimal GetSequence(string tabela)
        {
            return _conn.Query<decimal>($@"select {tabela}.nextval from dual").FirstOrDefault();
        }

        public IEnumerable<PerfilAlcadaGrupoEstoque> GetGrupoEstoqueAlcada(PesoGrupo pesoGrupo)
        {
            return _conn.Query<PerfilAlcadaGrupoEstoque>(@"select t.cpa_in_codigo     as CodigoPerfil
                                                                 ,t.gru_tab_in_codigo as GrupoTabela 
                                                                 ,t.gru_pad_in_codigo as GrupoPadrao 
                                                                 ,t.gru_ide_st_codigo as GrupoIdentificador
                                                                 ,t.gru_in_codigo     as CodigoGrupo 
                                                            from mgglo.WGLO_PERFIL_ALCADA_GRUPO_EST t
                                                           where  t.gru_tab_in_codigo = :GrupoTabela
                                                             and t.gru_pad_in_codigo = :GrupoPadrao
                                                             and t.gru_ide_st_codigo = :GrupoIdentificador
                                                             and t.gru_in_codigo = :GrupoCodigo"
                                                   , new { GrupoTabela = pesoGrupo.GrupoTabela,
                                                           GrupoPadrao = pesoGrupo.GrupoPadrao,
                                                           GrupoIdentificador = pesoGrupo.GrupoIdentificador,
                                                           GrupoCodigo = pesoGrupo.GrupoCodigo});
        }
    }
}
