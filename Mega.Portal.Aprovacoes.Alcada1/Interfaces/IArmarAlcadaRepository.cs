﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mega.Portal.Aprovacoes.ArmarAlcadas.Models;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Interfaces
{
    public interface IArmarAlcadaRepository
    {
        //Contrato GetContrato(Contrato contrato);
        //IEnumerable<Contrato> Get10();
        //IEnumerable<ContratoItem> GetContratoItens(Contrato contrato);
        IEnumerable<ClasseExcecao> GetClasseExcecoes();
        IEnumerable<RateioBloco> GetRateiosBloco(Contrato contrato);
        IEnumerable<PerfilAlcada> GetPerfilAlcada();
        IEnumerable<PerfilAlcadaVigencia> GetVigencia(decimal codigoPerfil);
        IEnumerable<PerfilAlcadaFaixa> GetFaixaValor(decimal codigoVigencia);
        IEnumerable<PerfilAlcadaExcecao> GetExcecao(decimal codigoFaixa);
        //GrupoEstoque GetGrupoEstoque(ContratoItem item);
        IEnumerable<PerfilAlcadaVinculoAplicacao> GetVinculoAplicacao();
        IEnumerable<PerfilAlcadaFilial> GetFilialAlcada(Contrato oContrato);
        decimal InsertAlcadaControle(PerfilAlcadaControle perfilControle);
        decimal GetSequence(string tabela);
        IEnumerable<PerfilAlcadaGrupoEstoque> GetGrupoEstoqueAlcada(PesoGrupo pesoGrupo);
    }
}
