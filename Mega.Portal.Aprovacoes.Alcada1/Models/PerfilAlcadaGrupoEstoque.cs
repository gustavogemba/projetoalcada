﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class PerfilAlcadaGrupoEstoque
    {
        public decimal CodigoPerfil { get; set; }
        public decimal GrupoTabela { get; set; }
        public decimal GrupoPadrao { get; set; }
        public string GrupoIdentificador { get; set; }
        public decimal CodigoGrupo { get; set; }
    }
}
