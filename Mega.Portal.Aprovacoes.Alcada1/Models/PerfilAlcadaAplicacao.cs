﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class PerfilAlcadaAplicacao
    {
        public decimal CodigoAplicacao { get; set; }
        public string Descricao { get; set; }
        public string ConsideraFilial { get; set; }
        public string ConsideraGrupoEstoque { get; set; }
        public decimal Modulo { get; set; }
        public decimal CodigoAplicacaoPai { get; set; }
    }
}
