﻿using Mega.Portal.Aprovacoes.ArmarAlcadas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class PerfilAlcadaExcecao
    {
        public decimal CodigoExcecao { get; set; }
        public decimal CodigoFaixa { get; set; }
        public decimal TotalAssinaturas { get; set; }
        public decimal ConsideraOrdemAprovacao { get; set; }
        public Projeto Projeto { get; set; }
        public CentroCusto CentroCusto { get; set; }
    }
}
