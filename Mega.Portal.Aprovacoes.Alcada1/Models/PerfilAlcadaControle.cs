﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class PerfilAlcadaControle
    {
        public decimal CodigoControle { get; set; }
        public decimal CodigoAplicacao { get; set; }
        public decimal CodigoFaixa { get; set; }
        public decimal? CodigoExcecao { get; set; }
        public decimal CodigoMovimento { get; set; }
        public decimal NivelAnterior { get; set; }
        public decimal NivelAtual { get; set; }
        public DateTime DataRegistro { get; set; }
        public decimal CodigoUsuarioWeb { get; set; }
        public decimal CodigoPerfil { get; set; }
        public string Parecer { get; set; }
        public string AprovacaoMultipla { get; set; }
        public decimal? CodigoPaiMovimento { get; set; }
        public decimal? CodigoFaixaGrupo { get; set; }
        public string Status { get; set; }
        public decimal CodigoUsuarioLiberacao { get; set; }
    }
}
