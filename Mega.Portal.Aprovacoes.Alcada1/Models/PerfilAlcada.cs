﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class PerfilAlcada
    {
        public decimal CodigoPerfil { get; set; }
        public string  Descricao { get; set; }
        public string  CancelarAprovacoes { get; set; }
        public string  TipoAprovacao { get; set; }
        public string  PerfilExcluido { get; set; }
        public IEnumerable<PerfilAlcadaVigencia> Vigencia { get; set; }
        public IEnumerable<PerfilAlcadaVinculoAplicacao> VinculoAplicacao { get; set; }
        public IEnumerable<PerfilAlcadaFilial> Filial { get; set; }
        public IEnumerable<PerfilAlcadaGrupoEstoque> GrupoEstoque { get; set; }
    }
}
