﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class PerfilAlcadaVigencia
    {
        public decimal CodigoVigencia { get; set; }
        public decimal CodigoAlcada { get; set; }
        public DateTime DataVigencia { get; set; }         
        public IEnumerable<PerfilAlcadaFaixa> Faixa { get; set; }
    }
}
