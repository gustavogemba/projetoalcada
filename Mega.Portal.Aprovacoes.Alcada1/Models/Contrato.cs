﻿using Mega.Portal.Aprovacoes.ArmarAlcadas.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Models
{
    public class Contrato : ChaveCompleta
    {
        public decimal Id { get; set; }
        public decimal Codigo { get; set; }
        public decimal Usuario { get; set; }
        public CentroCusto CentroCusto { get; set; }
        public Projeto Projeto { get; set; }
        public decimal Filial { get; set; }
        public string Origem { get; set; }
    }
}
