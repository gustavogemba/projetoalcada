﻿using FluentAssertions;
using Mega.Erp.Empreiteiros.Contratos.Models;
using Mega.Portal.Aprovacoes.Alcadas.Core;
using Mega.Portal.Aprovacoes.Alcadas.Models;
using Mega.Portal.Aprovacoes.ArmarAlcadas.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Mega.Portal.Aprovacoes.Alcadas.Test
{
    public class ValidacoesAlcadaTest
    {
        ObjetosParaTestes objetos = new ObjetosParaTestes();
        private ContratoItem[] _listaItem;
        private ClasseExcecao[] _listaExcecao;
        private ClasseExcecao[] _listaExcecao2;
        private PerfilAlcadaVigencia[] _alcadaVigencia;
        private PerfilAlcadaVigencia[] _alcadaVigencia2;
        private PerfilAlcadaFaixa[] _alcadaFaixa;
        private PerfilAlcadaExcecao[] _listaAlcadaExcecao;
        private PerfilAlcada[] _listaPerfilAlcada;
        private PerfilAlcada[] _listaPerfis;
        private PerfilAlcada[] _listaPerfis2;
        private PerfilAlcadaVinculoAplicacao[] _listaVinculoAplicacao;
        private PerfilAlcadaVinculoAplicacao[] _listaVinculoAplicacao2;
        private PerfilAlcadaFilial[] _listaFilial;
        private PerfilAlcadaGrupoEstoque[] _listaGrupoEstoque;

        public ValidacoesAlcadaTest()
        {
            _listaItem = objetos.CarregarListaItens();
            _listaExcecao = objetos.CarregarClasseExcecao();
            _listaExcecao2 = objetos.CarregarClasseExcecao2();
            _alcadaVigencia = objetos.CarregarListaVigenciaAlcada();
            _alcadaVigencia2 = objetos.CarregarListaVigenciaAlcada2();
            _alcadaFaixa = objetos.CarregarListaAlcadaFaixa();
            _listaAlcadaExcecao = objetos.CarregarListaAlcadaExcecao();
            _listaPerfilAlcada = objetos.CarregarListaPerfilAlcada();
            _listaVinculoAplicacao = objetos.CarregarListaVinculoAplicacao();
            _listaFilial = objetos.CarregarListaFilial();
            _listaGrupoEstoque = objetos.CarregarListaGruposEstoque();
            _listaPerfis = objetos.CarregarListaPerfis();
            _listaPerfis2 = objetos.CarregarListaPerfis2();
            _listaVinculoAplicacao2 = objetos.CarregarListaVinculoAplicacao2();
        }

        [Fact]
        public void TesteValidaExcecaoClasseItem()
        {
            bool temExcecao = new ValidacoesAlcada().ValidaExcecaoClasseItem(_listaItem, _listaExcecao);
            temExcecao.Should().BeTrue();
        }

        [Fact]
        public void TesteValidaExcecaoClasseItemFalse()
        {
            bool temExcecao = new ValidacoesAlcada().ValidaExcecaoClasseItem(_listaItem, _listaExcecao2);
            temExcecao.ShouldBeEquivalentTo(false);
        }

        [Theory]
        [InlineData(3000)]
        public void TesteValidaTotalContrato(decimal valorEsperado)
        {
            decimal total = Calculos.Instance.TotalContrato(_listaItem);
            total.ShouldBeEquivalentTo(valorEsperado);
        }


        [Theory]
        [InlineData(0, 0)]
        [InlineData(1, 1)]
        [InlineData(500, 1)]
        [InlineData(1000, 1)]
        [InlineData(200001, 0)]
        public void RecuperaFaixaValidaTest(decimal valorMovimento, decimal valorEsperado)
        {
            decimal codigoFaixa = ValidacoesAlcada.Instance.RecuperaFaixaValida(_alcadaFaixa, valorMovimento);
            codigoFaixa.ShouldBeEquivalentTo(valorEsperado);
        }

        [Fact]
        public void RecuperarVigenciaValidaTest()
        {
            decimal codigoVigencia = ValidacoesAlcada.Instance.RecuperarVigenciaValida(_alcadaVigencia);
            codigoVigencia.ShouldBeEquivalentTo(2);
        }

        [Fact]
        public void RecuperarVigenciaInvalidaTest()
        {
            decimal codigoVigencia = ValidacoesAlcada.Instance.RecuperarVigenciaValida(_alcadaVigencia2);
            codigoVigencia.ShouldBeEquivalentTo(0);
        }

        [Theory]
        [InlineData(57, 1, "1", 13, 2, 1, "1", 5, 1)]
        [InlineData(57, 1, "1", 14, 2, 1, "1", 6, 2)]
        [InlineData(57, 1, "1", 15, 2, 1, "1", 7, 3)]
        //[InlineData(57, 1, "1", 16, 2, 1, "1", 8, null)]
        public void RecuperaExcecaoValidaTest(decimal projetoTabela, decimal projetoPadrao, string projetoIdentificador, decimal projetoReduzido,
                                              decimal cCustoTabela, decimal cCustoPadrao, string cCustoIdentificador, decimal cCustoReduzido,
                                              decimal valorEsperado)
        {
            Projeto projeto = new Projeto { ProjetoTabela = projetoTabela, ProjetoPadrao = projetoPadrao, ProjetoIdentificador = projetoIdentificador, ProjetoReduzido = projetoReduzido };
            CentroCusto centroCusto = new CentroCusto { CCustoTabela = cCustoTabela, CCustoPadrao = cCustoPadrao, CCustoIdentificador = cCustoIdentificador, CCustoReduzido = cCustoReduzido };
            decimal? codigoExcecao = ValidacoesAlcada.Instance.RecuperaExcecaoValida(centroCusto, projeto, _listaAlcadaExcecao);
            codigoExcecao.ShouldBeEquivalentTo(valorEsperado);
        }

        [Fact]
        public void PerfisDeAlcadaAptosTest()
        {
            PerfilAlcada perfil = ValidacoesAlcada.Instance.PerfisDeAlcadaAptos(_listaPerfilAlcada, _listaVinculoAplicacao, _listaFilial, _listaGrupoEstoque);
            perfil.ShouldBeEquivalentTo(new PerfilAlcada { CodigoPerfil = 1, Descricao = "Perfil A", CancelarAprovacoes = "S", TipoAprovacao = "U" });
        }

        [Theory]
        [InlineData(1, 2)]
        [InlineData(2, 0)]
        public void RecuperaPerfisValidosTest(decimal lista, decimal numeroPerfisValidos)
        {
            IEnumerable<PerfilAlcada> lPerfisValidos = ValidacoesAlcada.Instance.RecuperaPerfisValidos(lista.Equals(1) ? _listaPerfis : _listaPerfis2);
            lPerfisValidos.Count().ShouldBeEquivalentTo(numeroPerfisValidos);
        }

        [Theory]
        [InlineData(2, 1)]
        [InlineData(4, 2)]
        [InlineData(5, 4)]
        [InlineData(6, 3)]
        [InlineData(7, 4)]
        [InlineData(8, 1)]
        [InlineData(9, 2)]
        [InlineData(10, 1)]
        [InlineData(11, 4)]
        [InlineData(13, 0)]
        public void RecuperaAplicacaoValidaTest(int aplicacao, decimal valorEsperado)
        {
            IEnumerable<PerfilAlcadaVinculoAplicacao> lViculoAplicacoesValidas = ValidacoesAlcada.Instance.RecuperaVinculoAplicacoesValida(_listaVinculoAplicacao2, aplicacao);
            lViculoAplicacoesValidas.Count().ShouldBeEquivalentTo(valorEsperado);
        }

    }
}
