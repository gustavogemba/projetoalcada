﻿using System;
using System.Collections.Generic;
using Mega.Erp.Empreiteiros.Contratos.Models;
using Mega.Portal.Aprovacoes.Alcadas.Interfaces;
using Mega.Portal.Aprovacoes.Alcadas.Models;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Test
{
    internal class AlcadaRepositoryMockup : IArmarAlcadaRepository
    {
        ObjetosParaTestes objetos = new ObjetosParaTestes();

        public AlcadaRepositoryMockup() 
        {
            
        }

        public IEnumerable<ClasseExcecao> GetClasseExcecoes()
        {
            return objetos.CarregarClasseExcecao();
        }

        public IEnumerable<PerfilAlcadaExcecao> GetExcecao(decimal codigoFaixa)
        {
            return objetos.CarregarListaAlcadaExcecao();
        }

        public IEnumerable<PerfilAlcadaFaixa> GetFaixaValor(decimal codigoVigencia)
        {
            return objetos.CarregarListaAlcadaFaixa();
        }

        public IEnumerable<PerfilAlcadaFilial> GetFilialAlcada(Contrato oContrato)
        {
            return objetos.CarregarListaFilial();
        }

        public IEnumerable<PerfilAlcadaGrupoEstoque> GetGrupoEstoqueAlcada(PesoGrupo pesoGrupo)
        {
            return objetos.CarregarListaGruposEstoque();
        }

        public IEnumerable<PerfilAlcada> GetPerfilAlcada()
        {
            return objetos.CarregarListaPerfis();
        }

        public IEnumerable<RateioBloco> GetRateiosBloco(Contrato contrato)
        {
            return objetos.CarregarListaBlocos();
        }

        public decimal GetSequence(string tabela)
        {
            return 1;
        }

        public IEnumerable<PerfilAlcadaVigencia> GetVigencia(decimal codigoPerfil)
        {
            return objetos.CarregarListaVigenciaAlcada();
        }

        public IEnumerable<PerfilAlcadaVinculoAplicacao> GetVinculoAplicacao()
        {
            return objetos.CarregarListaVinculoAplicacao2();
        }

        public decimal InsertAlcadaControle(PerfilAlcadaControle perfilControle)
        {
            return 1;
        }
    }
}