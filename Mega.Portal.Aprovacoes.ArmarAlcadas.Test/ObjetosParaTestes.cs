﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mega.Erp.Empreiteiros.Contratos.Models;
using Mega.Portal.Aprovacoes.Alcadas.Models;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Test
{
    public class ObjetosParaTestes
    {
        internal ContratoItem[] CarregarListaItens()
        {
            return new ContratoItem[] {
                new ContratoItem { Id= 6125, Contrato=2604, Item=6125, Produto = new Produto(){ ProdutoTabela=100, ProdutoPadrao = 1, ProdutoCodigo=389 }, Valor=500, Situacao="A", Liberado="N", Classe = new Classe(){ClasseTabela=350, ClassePadrao=1, ClasseIdentificador="2", ClasseReduzido=133  }, AprovaAlcada="AA", },
                new ContratoItem { Id= 6120, Contrato=2604, Item=6120, Produto = new Produto(){ ProdutoTabela=100, ProdutoPadrao = 1, ProdutoCodigo=390 }, Valor=1000, Situacao="A", Liberado="N", Classe = new Classe(){ClasseTabela=350, ClassePadrao=1, ClasseIdentificador="2", ClasseReduzido=134  }, AprovaAlcada="AA" },
                new ContratoItem { Id= 6121, Contrato=2604, Item=6121, Produto = new Produto(){ ProdutoTabela=100, ProdutoPadrao = 1, ProdutoCodigo=391 }, Valor=1500, Situacao="A", Liberado="N", Classe = new Classe(){ClasseTabela=350, ClassePadrao=1, ClasseIdentificador="2", ClasseReduzido=135  }, AprovaAlcada="AA" }
            };
        }

        internal ClasseExcecao[] CarregarClasseExcecao()
        {
            return new ClasseExcecao[]
          {
                new ClasseExcecao {CodigoExcecao=1, ClasseTabela=350, ClassePadrao=1, ClasseIdentificador="2" , ClasseReduzido=133 },
                new ClasseExcecao {CodigoExcecao=2, ClasseTabela=350, ClassePadrao=1, ClasseIdentificador="2" , ClasseReduzido=400 },
                new ClasseExcecao {CodigoExcecao=3, ClasseTabela=350, ClassePadrao=1, ClasseIdentificador="2" , ClasseReduzido=500 }
          };
        }

        internal IEnumerable<RateioBloco> CarregarListaBlocos()
        {
            return new RateioBloco[]
            {
                new RateioBloco{ CodigoTabela = 53, CodigoPadrao = 1, CodigoOrganizacao = 496, CodigoAuxiliar = "G", CodigoContrato = 10949,
                    Projeto = new Projeto{ProjetoTabela=57, ProjetoPadrao=1, ProjetoIdentificador="8", ProjetoReduzido=81},
                    CentroCusto = new CentroCusto{CCustoTabela=2, CCustoPadrao=1, CCustoIdentificador="2", CCustoReduzido=22} }
            };
        }

        internal PerfilAlcadaFilial[] CarregarListaFilial()
        {
            return new PerfilAlcadaFilial[]
            {
                new PerfilAlcadaFilial { CodigoPerfil = 1, CodigoTabela = 53, CodigoPadrao = 1, CodigoOrganizacao = 4, CodigoAuxiliar = "G" },
                new PerfilAlcadaFilial { CodigoPerfil = 1, CodigoTabela = 53, CodigoPadrao = 1, CodigoOrganizacao = 5, CodigoAuxiliar = "G" },
                new PerfilAlcadaFilial { CodigoPerfil = 1, CodigoTabela = 53, CodigoPadrao = 1, CodigoOrganizacao = 497, CodigoAuxiliar = "G" },
                new PerfilAlcadaFilial { CodigoPerfil = 2, CodigoTabela = 53, CodigoPadrao = 1, CodigoOrganizacao = 10, CodigoAuxiliar = "G" },
                new PerfilAlcadaFilial { CodigoPerfil = 3, CodigoTabela = 53, CodigoPadrao = 1, CodigoOrganizacao = 497, CodigoAuxiliar = "G" },
                new PerfilAlcadaFilial { CodigoPerfil = 4, CodigoTabela = 53, CodigoPadrao = 1, CodigoOrganizacao = 497, CodigoAuxiliar = "G" }
            };
        }

        internal PerfilAlcadaVinculoAplicacao[] CarregarListaVinculoAplicacao2()
        {
            return new PerfilAlcadaVinculoAplicacao[]
            {
                // Perfil 1
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 1, AplicacaoAlcada = 2 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 1, AplicacaoAlcada = 4 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 1, AplicacaoAlcada = 5 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 1, AplicacaoAlcada = 6 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 1, AplicacaoAlcada = 7 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 1, AplicacaoAlcada = 8 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 1, AplicacaoAlcada = 11 },
                // Perfil 2
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 2, AplicacaoAlcada = 5 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 2, AplicacaoAlcada = 6 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 2, AplicacaoAlcada = 7 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 2, AplicacaoAlcada = 11 },
                // Perfil 3
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 3, AplicacaoAlcada = 5 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 3, AplicacaoAlcada = 6 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 3, AplicacaoAlcada = 7 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 3, AplicacaoAlcada = 9 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 3, AplicacaoAlcada = 11 },
                // Perfil 4
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 4, AplicacaoAlcada = 4 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 4, AplicacaoAlcada = 5 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 4, AplicacaoAlcada = 7 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 4, AplicacaoAlcada = 9 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 4, AplicacaoAlcada = 10 },
                new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 4, AplicacaoAlcada = 11 }
                //new PerfilAlcadaVinculoAplicacao { CodigoAlcada = 4, AplicacaoAlcada = 13 },
            };
        }

        internal PerfilAlcada[] CarregarListaPerfis2()
        {
            return new PerfilAlcada[]
            {
                // Perfis TipoAprovacao "G" alternando CancelarAprovacoes ("S" / "N) e PerfilExcluido ("S" / "N)
                new PerfilAlcada { CodigoPerfil = 1, Descricao = "Perfil 1", CancelarAprovacoes = "N", TipoAprovacao = "G", PerfilExcluido = "S" },
                new PerfilAlcada { CodigoPerfil = 2, Descricao = "Perfil 2", CancelarAprovacoes = "S", TipoAprovacao = "G", PerfilExcluido = "S" },
                new PerfilAlcada { CodigoPerfil = 3, Descricao = "Perfil 3", CancelarAprovacoes = "N", TipoAprovacao = "G", PerfilExcluido = "N" },
                new PerfilAlcada { CodigoPerfil = 4, Descricao = "Perfil 4", CancelarAprovacoes = "S", TipoAprovacao = "G", PerfilExcluido = "N" },
            };
        }

        internal PerfilAlcada[] CarregarListaPerfis()
        {
            return new PerfilAlcada[]
            {
                // Perfis TipoAprovacao "G" alternando CancelarAprovacoes ("S" / "N) e PerfilExcluido ("S" / "N)
                new PerfilAlcada { CodigoPerfil = 1, Descricao = "Perfil 1", CancelarAprovacoes = "N", TipoAprovacao = "G", PerfilExcluido = "S" },
                new PerfilAlcada { CodigoPerfil = 2, Descricao = "Perfil 2", CancelarAprovacoes = "S", TipoAprovacao = "G", PerfilExcluido = "S" },
                new PerfilAlcada { CodigoPerfil = 3, Descricao = "Perfil 3", CancelarAprovacoes = "N", TipoAprovacao = "G", PerfilExcluido = "N" },
                new PerfilAlcada { CodigoPerfil = 4, Descricao = "Perfil 4", CancelarAprovacoes = "S", TipoAprovacao = "G", PerfilExcluido = "N" },
                // Perfis TipoAprovacao "U" alternando CancelarAprovacoes ("S" / "N) e PerfilExcluido ("S" / "N)
                new PerfilAlcada { CodigoPerfil = 1, Descricao = "Perfil 1", CancelarAprovacoes = "N", TipoAprovacao = "U", PerfilExcluido = "S" },
                new PerfilAlcada { CodigoPerfil = 2, Descricao = "Perfil 2", CancelarAprovacoes = "S", TipoAprovacao = "U", PerfilExcluido = "S" },
                new PerfilAlcada { CodigoPerfil = 3, Descricao = "Perfil 3", CancelarAprovacoes = "N", TipoAprovacao = "U", PerfilExcluido = "N" },
                new PerfilAlcada { CodigoPerfil = 4, Descricao = "Perfil 4", CancelarAprovacoes = "S", TipoAprovacao = "U", PerfilExcluido = "N" },
            };
        }

        internal PerfilAlcadaGrupoEstoque[] CarregarListaGruposEstoque()
        {
            return new PerfilAlcadaGrupoEstoque[]
            {
                new PerfilAlcadaGrupoEstoque{ CodigoPerfil = 1, GrupoTabela = 101, GrupoPadrao = 1, GrupoIdentificador = "1", CodigoGrupo = 59},
                new PerfilAlcadaGrupoEstoque{ CodigoPerfil = 1, GrupoTabela = 101, GrupoPadrao = 1, GrupoIdentificador = "1", CodigoGrupo = 60},
                new PerfilAlcadaGrupoEstoque{ CodigoPerfil = 2, GrupoTabela = 101, GrupoPadrao = 1, GrupoIdentificador = "1", CodigoGrupo = 61},
                new PerfilAlcadaGrupoEstoque{ CodigoPerfil = 2, GrupoTabela = 101, GrupoPadrao = 1, GrupoIdentificador = "1", CodigoGrupo = 63}
            };
        }

        internal PerfilAlcadaVinculoAplicacao[] CarregarListaVinculoAplicacao()
        {
            return new PerfilAlcadaVinculoAplicacao[]
            {
                new PerfilAlcadaVinculoAplicacao{CodigoAlcada = 1, AplicacaoAlcada = 4 },
                new PerfilAlcadaVinculoAplicacao{CodigoAlcada = 1, AplicacaoAlcada = 5 },
                new PerfilAlcadaVinculoAplicacao{CodigoAlcada = 1, AplicacaoAlcada = 6 },
                new PerfilAlcadaVinculoAplicacao{CodigoAlcada = 2, AplicacaoAlcada = 4 },
                new PerfilAlcadaVinculoAplicacao{CodigoAlcada = 2, AplicacaoAlcada = 5 }
            };
        }

        internal PerfilAlcada[] CarregarListaPerfilAlcada()
        {
            return new PerfilAlcada[]
            {
                new PerfilAlcada{CodigoPerfil=1, Descricao="Perfil A", CancelarAprovacoes="S", TipoAprovacao="U" },
                new PerfilAlcada{CodigoPerfil=2, Descricao="Perfil B", CancelarAprovacoes="N", TipoAprovacao="U" },
                new PerfilAlcada{CodigoPerfil=3, Descricao="Perfil C", CancelarAprovacoes="S", TipoAprovacao="G" }
            };
        }

        internal PerfilAlcadaExcecao[] CarregarListaAlcadaExcecao()
        {
            return new PerfilAlcadaExcecao[]
            {
                new PerfilAlcadaExcecao { CodigoExcecao=1, Projeto = new Projeto{ProjetoTabela=57, ProjetoPadrao=1, ProjetoIdentificador="1", ProjetoReduzido=13}, CentroCusto = new CentroCusto{CCustoTabela=2, CCustoPadrao=1, CCustoIdentificador="1", CCustoReduzido=5} },
                new PerfilAlcadaExcecao { CodigoExcecao=2, Projeto = new Projeto{ProjetoTabela=57, ProjetoPadrao=1, ProjetoIdentificador="1", ProjetoReduzido=14}, CentroCusto = new CentroCusto{CCustoTabela=2, CCustoPadrao=1, CCustoIdentificador="1", CCustoReduzido=6} },
                new PerfilAlcadaExcecao { CodigoExcecao=3, Projeto = new Projeto{ProjetoTabela=57, ProjetoPadrao=1, ProjetoIdentificador="1", ProjetoReduzido=15}, CentroCusto = new CentroCusto{CCustoTabela=2, CCustoPadrao=1, CCustoIdentificador="1", CCustoReduzido=7} }
            };
        }

        internal PerfilAlcadaFaixa[] CarregarListaAlcadaFaixa()
        {
            return new PerfilAlcadaFaixa[]
            {
                new PerfilAlcadaFaixa {CodigoFaixa = 1,CodigoVigencia=2,ConsideraOrdemAprovacao="S",TotalAssinaturas=2,ValorInicial= 1 , ValorFinal = 1000},
                new PerfilAlcadaFaixa {CodigoFaixa = 2,CodigoVigencia=2,ConsideraOrdemAprovacao="S",TotalAssinaturas=2,ValorInicial= 1001, ValorFinal = 10000},
                new PerfilAlcadaFaixa {CodigoFaixa = 3,CodigoVigencia=2,ConsideraOrdemAprovacao="S",TotalAssinaturas=2,ValorInicial= 10001, ValorFinal = 200000}
            };
        }

        internal PerfilAlcadaVigencia[] CarregarListaVigenciaAlcada2()
        {
            return new PerfilAlcadaVigencia[]
            {
                new PerfilAlcadaVigencia {CodigoAlcada = 1, CodigoVigencia=1, DataVigencia=Convert.ToDateTime("01/08/2017")},
                new PerfilAlcadaVigencia {CodigoAlcada = 1, CodigoVigencia=2, DataVigencia=Convert.ToDateTime("02/08/2017")},
                new PerfilAlcadaVigencia {CodigoAlcada = 1, CodigoVigencia=3, DataVigencia=Convert.ToDateTime("03/08/2017")}
            };
        }

        internal PerfilAlcadaVigencia[] CarregarListaVigenciaAlcada()
        {
            return new PerfilAlcadaVigencia[]
            {
                new PerfilAlcadaVigencia {CodigoAlcada = 1, CodigoVigencia=1, DataVigencia=Convert.ToDateTime("01/06/2017")},
                new PerfilAlcadaVigencia {CodigoAlcada = 1, CodigoVigencia=2, DataVigencia=Convert.ToDateTime("02/06/2017")},
                new PerfilAlcadaVigencia {CodigoAlcada = 1, CodigoVigencia=3, DataVigencia=Convert.ToDateTime("03/07/2017")}
            };
        }

        internal ClasseExcecao[] CarregarClasseExcecao2()
        {
            return new ClasseExcecao[]
             {
                new ClasseExcecao {CodigoExcecao=1, ClasseTabela=350, ClassePadrao=1, ClasseIdentificador="2" , ClasseReduzido=130 },
                new ClasseExcecao {CodigoExcecao=2, ClasseTabela=350, ClassePadrao=1, ClasseIdentificador="2" , ClasseReduzido=400 },
                new ClasseExcecao {CodigoExcecao=3, ClasseTabela=350, ClassePadrao=1, ClasseIdentificador="2" , ClasseReduzido=500 }
             };
        }
    }
}
