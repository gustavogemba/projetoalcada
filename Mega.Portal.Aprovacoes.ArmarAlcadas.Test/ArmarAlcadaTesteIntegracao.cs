﻿using FluentAssertions;
using Mega.Erp.Empreiteiros.Contratos.Models;
using Mega.Erp.Empreiteiros.Contratos.Service;
using Mega.Portal.Aprovacoes.Alcadas.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Test
{
    public class ArmarAlcadaTesteIntegracao
    {
        private ArmarAlcadaService _alcada;
        private ContratoService _contrato;
        private ContratoRepositoryMockup _repoContrato;
        private AlcadaRepositoryMockup _repoAlcada;

        public ArmarAlcadaTesteIntegracao()
        {
            _repoContrato = new ContratoRepositoryMockup();
            _contrato = new ContratoService(_repoContrato);
            _repoAlcada = new AlcadaRepositoryMockup();
            _alcada = new ArmarAlcadaService(_repoAlcada, _contrato);
        }

        [Fact]
        public void ArmarAlcadaContratoVazio()
        {
            Contrato cto = new Contrato();

            decimal ret = _alcada.ArmarAlcadaContrato(cto);

            ret.Should().Equals(0);
        }

        [Fact]
        public void ArmarAlcadaContratoPopulado()
        {
            Contrato cto = new Contrato()
            {
                Id = 0,
                Codigo = 10949,
                Usuario = 1,
                Filial = 497,
                Origem = "E",
                CodigoTabela = 53,
                CodigoPadrao = 1,
                CodigoOrganizacao = 496,
                CodigoAuxiliar = "G"
            };
            decimal ret = _alcada.ArmarAlcadaContrato(cto);
            ret.Should().NotBe(0);
        }

        [Fact]
        public void ArmarAlcadaContratoPopuladoComCtoVazio()
        {
            Contrato cto = new Contrato()
            {
                Id = 0,
                Codigo = 10949,
                Usuario = 1,
                Filial = 497,
                Origem = "E",
                CodigoTabela = 53,
                CodigoPadrao = 1,
                CodigoOrganizacao = 496,
                CodigoAuxiliar = "G"
            };
            decimal ret = _alcada.ArmarAlcadaContrato(cto);
            ret.Should().NotBe(0);
        }
    }
}
