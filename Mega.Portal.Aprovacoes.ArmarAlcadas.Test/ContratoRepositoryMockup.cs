﻿using System;
using System.Collections.Generic;
using Mega.Erp.Empreiteiros.Contratos.Interfaces;
using Mega.Erp.Empreiteiros.Contratos.Models;

namespace Mega.Portal.Aprovacoes.ArmarAlcadas.Test
{
    internal class ContratoRepositoryMockup : IContratoRepository
    {
        public ContratoRepositoryMockup()
        {
        }

        public Contrato GetContrato(Contrato contrato)
        {
            Contrato cto = new Contrato()
            {
                Id = 10949,
                Codigo = 10949,
                Usuario = 31,
                CentroCusto = new CentroCusto() { CCustoTabela = 2, CCustoPadrao = 1, CCustoReduzido = 22, CCustoIdentificador = "2" },
                Projeto = new Projeto() { ProjetoTabela = 57, ProjetoPadrao = 1, ProjetoIdentificador = "8", ProjetoReduzido = 81 },
                Filial = 497,
                Origem = "E",
                CodigoTabela = 53,
                CodigoPadrao = 1,
                CodigoOrganizacao = 496,
                CodigoAuxiliar = "G",
            };

            return cto;
        }

        public GrupoEstoque GetGrupoEstoque(ContratoItem item)
        {
            return new GrupoEstoque { GrupoTabela = 101, GrupoPadrao = 1, GrupoIdentificador = "1", GrupoCodigo = 999 };
        }

        public IEnumerable<ContratoItem> GetItensContrato(Contrato contrato)
        {
            return new ContratoItem[] {
                    new ContratoItem { Id = 30336,
                                       Contrato = 10949,
                                       Item =30336,
                                       Aditivo = 0,
                                       Valor= 5613,
                                       Situacao = "A",
                                       Liberado ="N",
                                       AprovaAlcada="AB",
                                       Produto = new Produto(){  ProdutoTabela =100, ProdutoPadrao=1,ProdutoCodigo=2},
                                       Classe = new Classe(){ ClasseTabela =350,ClassePadrao=1, ClasseIdentificador="2",ClasseReduzido=133 },
                                       CodigoTabela = 53,
                                       CodigoPadrao = 1,
                                       CodigoOrganizacao = 496,
                                       CodigoAuxiliar = "G"
                    },
                    new ContratoItem { Id = 30335,
                                       Contrato = 10949,
                                       Item =30335,
                                       Aditivo = 0,
                                       Valor= 1223,
                                       Situacao = "A",
                                       Liberado ="N",
                                       AprovaAlcada="AB",
                                       Produto = new Produto(){  ProdutoTabela =100, ProdutoPadrao=1,ProdutoCodigo=8},
                                       Classe = new Classe(){ ClasseTabela =350,ClassePadrao=1, ClasseIdentificador="2",ClasseReduzido=133 },
                                       CodigoTabela = 53,
                                       CodigoPadrao = 1,
                                       CodigoOrganizacao = 496,
                                       CodigoAuxiliar = "G"
                    },
                    new ContratoItem { Id = 30337,
                                       Contrato = 10949,
                                       Item =30337,
                                       Aditivo = 0,
                                       Valor= 130000,
                                       Situacao = "A",
                                       Liberado ="N",
                                       AprovaAlcada="AB",
                                       Produto = new Produto(){  ProdutoTabela =100, ProdutoPadrao=1,ProdutoCodigo=11},
                                       Classe = new Classe(){ ClasseTabela =350,ClassePadrao=1, ClasseIdentificador="2",ClasseReduzido=133 },
                                       CodigoTabela = 53,
                                       CodigoPadrao = 1,
                                       CodigoOrganizacao = 496,
                                       CodigoAuxiliar = "G"
                    }
                };
        }
    }
}