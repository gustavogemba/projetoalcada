﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System.Data.Common;
using Oracle.ManagedDataAccess.Client;
using Mega.Erp.Empreiteiros.Contratos.Repository;
using Mega.Erp.Empreiteiros.Contratos.Interfaces;
using Mega.Erp.Empreiteiros.Contratos.Service;
using Mega.Portal.Aprovacoes.Alcadas.Services;
using Mega.Portal.Aprovacoes.Alcadas.Repository;
using Mega.Portal.Aprovacoes.Alcadas.Interfaces;

namespace Mega.Portal.Sevidor
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddScoped<DbConnection>(_ =>
            {
                var connect = new OracleConnection("user id=mega;password=megamega;data source=192.130.130.3:1521/DSNV");
                connect.Open();
                return connect;
            });
            services.AddScoped<ArmarAlcadaService>();
            services.AddScoped<IArmarAlcadaRepository, ArmarAlcadaRepository>();
            services.AddScoped<ContratoService>();
            services.AddScoped<IContratoRepository, ContratoRepository>();

            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
        }
    }
}
